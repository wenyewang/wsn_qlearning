
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>

#pragma pack(1)

#ifndef INT_MAX
	#define INT_MIN     (-2147483647 - 1) /* minimum (signed) int value */
	#define INT_MAX       2147483647    /* maximum (signed) int value */
#endif

#ifndef M_2_PI		// 2 / PI
	#define M_2_PI     0.636619772367581343076
#endif

#define RAND()				20//rand

#define WIDTH				500     // width of the area where sensor nodes exist
#define HEIGHT				500     // height of the area where sensor nodes exist
#define ROUND				5000
#define N					50      // num of node

#define LEACH_P				0.2     // P arguments in LEACH used to choose Cluster Header
#define ROUND_P				(int)(1 / LEACH_P)

#define INVALID_V			(-1)
#define PACK_INIT_RATIO		80      // (PACK_INIT_RATIO / 100) define the probability of one node which has pack to transfer
#define MAX_PACKET_SIZE		40      // max size of arrived packets to nodes
#define MIN_ENERGY_CH		GET_E_init(i) * 0.2		// minimal energy needed to be a cluster head
#define MIN_BUFSIZ_CH		GET_BUF_init(i) * 0.2	// minimal remain buffer size needed to be a cluster head
#define MAX_TRANSFER_DIS	100

#define	G					1       // used to calculate Q
#define A1					0.5     // used to calculate Q
#define A2					0.5
#define B1					0.5     // used to calculate Q
#define B2					0.05
#define C(i)				(1 - node[i].energy / GET_E_init(i))		// 1-Residual Energy (sn)/Einitial (sn)
#define D(i)				(M_2_PI) * atan(node[i].energy - node[i].neighborMeanEnergy)
#define GET_Qsucc(a, b)		((double)-G - (double)A1 * ((double)C(a) + (double)C(b)) + (double)A2 * ((double)D(a) + (double)D(b)))				//- G - A1 * (C(i) + C(j));
#define GET_Qfail(a)		(((double)-G - (double)B1 * (double)C(a)) + B2 * D(a))

#define GET_E_init(i)				50			// get initial energy of node[i], could be set separately by initEnergy[i]
#define GET_BUF_init(i)				100         // get initial buffer size of node[i], could be set separately by initBuffer[i]
#define GET_C_elec(i)				5e-5//0.3         // get electronic arguments of node[i], could be set separately by eConsumption[i]
#define GET_C_amp(i)				1e-11//0.0002        // get amplifier arguments of node[i], could be set separately by ampConsumption[i]

#define GET_DISTANCE(from, to)		(int)(sqrt(pow(node[from].x - node[to].x, 2) + pow(node[from].y - node[to].y, 2)))     // distance from node "from" to node "to"
#define GET_DISTANCE_TO_SINK(from)	(int)(sqrt(pow(node[from].x - sinkNode.x, 2) + pow(node[from].y - sinkNode.y, 2)))     // distance from node "from" to sink node
#define GET_CLUSTER_DIST(i)			(int)(node[i].cluster_dis)       // distance from node[i] to its cluster header node
#define ETX(from, nbits)			(double)(nbits * 8 * GET_C_elec(from) + nbits * 8 * GET_C_amp(from) * pow(GET_CLUSTER_DIST(from), 2))	// energy cost by transfer nbits from node "from" to its CH
#define ETX_TO_SINK(from, nbits)	(double)(nbits * 8 * GET_C_elec(from) + nbits * 8 * GET_C_amp(from) * pow(GET_DISTANCE_TO_SINK(from), 2))  // energy cost by transfer nbits from node "from" to sink node
#define ERX(i, nbits)				(double)(nbits * 8 * GET_C_elec(i))     //energy cost in node i by receiving nbits

#define GET_CLUSTER_HEAD(i)			node[cluster_id[node[i].cluster_no]]
#define GET_CLUSTER_POS(i)          cluster_id[node[i].cluster_no]
#define GET_REMAIN_BUFSIZ(i)		(node[i].bufferSize - node[i].cqsize)

//****** energy of nodes ******
/*
int initEnergy[N];          // init energy of every node, currently not used
int initBuffer[N];          // init buffer of every node, currently not used
double eConsumption[N];     // electronic consumption argument of every node, currently not used
double ampConsumption[N];	// amplifier consumption argument of every node, currently not used
*/
//*************************************


//****** LEACH ALgorithm ******
typedef enum Type {
	NODE, CLUSTER_HEAD
}Type;
typedef struct Node {
    double energy;              // residual energy
	double neighborMeanEnergy;	//
	int neighborCnt;			//
    int bufferSize;             // size of buffer
    int cqsize;                 // size of content, aka remain buffer size = bufferSize - cqsize;
    int x, y;                   // coordinate of this node

    Type type;                  // normal node or CH node
    int cluster_no;             // if this node is a normal node, the cluster_no shows the CH no. else equals INVALID_V
                                // Note. cluster_id[cluster_no] tell the Suffix of CH node.

    int cluster_dis;            // if this node is a normal node, the cluster_dis shows the distance to CH node. else equals INVALID_V
    bool isLive;                // whether the node is live
}Node;

Node sinkNode;          // sink node, only use its coordinate
Node node[N];
int packToTransfer[N];	// arrival packet size of each node, recalculate in each round. (PACK_INIT_RATIO / 100) define the probability.
int cluster_id[N];      // remember the CH node num of each Cluster. if node of Cluster_1 is node 5, cluster_id[1] = 5.
int cluster_cnt;        // number of Clusters
int cCnt[N];            // used to remember the number of normal node for each CH node. exp. cCnt[0] = 5 means "5 normal nodes choose CH0 as relay"
//*************************************


//**** used for statics ****
double tx_energy[N];       // tx energy of each node
double rx_energy[N];       // rx energy of each node
int bits_send[N];       // bits sent by each node
int LEACH_dead_Cnt = 0;     // number of dead node.

long totalArrivalDataSize = 0;		// bit size arrived to normal node, aka total pack arrive in wsn.
long totalArriveToSinkSize = 0;      // bit size successfully transfered to sink node.
long totalLostPacketInArrive = 0;    // bit size lost in arrived, means, didn't go into normal node at all because of full buffer
long totalLostPacketInTransfer = 0;  // bit size lost in transfer from normal node to CH node, or from CH node to sink

double meanResidualEnergy = 0;
double totalTransferEnergy = 0;        // total energy cost in Transfer
double totalReceiveEnergy = 0;         // total energy cost in Receive
//*****************************

FILE *fresult;
FILE *fanalysis;

void Init_OutFile()
{
	fopen_s(&fresult, "Result.dat", "w");
	fopen_s(&fanalysis, "Analysis.dat", "w");

	fprintf(fresult, "round\t throughput\t energyEfficiency \t meanResidualEnergy\n");
}

void Clost_OutFile()
{
	fclose(fresult);
	fclose(fanalysis);
}

/*
 * @brief Update Mean Redisual Energy to meanResidualEnergy
 */
void updateMeanResidualEnergy()
{
	meanResidualEnergy = 0;
	for(int t = 0; t < N; t++) {
		meanResidualEnergy += node[t].energy;
	}
	meanResidualEnergy /= N;
}

/*
 * @brief Update node i's neighbor 's Mean Redisual Energy
 */
void updateNeighborMeanResEnergy(int nodeNum)
{
	double resEnergySum = 0, t;
	int cnt = 0;
	for(int i = 0; i < N; i++) {
		if(i != nodeNum) {
			t = GET_DISTANCE(nodeNum, i);
			if(t < MAX_TRANSFER_DIS) {
				resEnergySum += t;
				cnt++;
			}
		}
	}
	node[nodeNum].neighborMeanEnergy = resEnergySum / cnt;
	node[nodeNum].neighborCnt = cnt;
}

void LEACH_Init(bool inRound)
{
	if(!inRound) {
        memset(tx_energy, 0, sizeof(tx_energy));
        memset(rx_energy, 0, sizeof(rx_energy));
        memset(bits_send, 0, sizeof(bits_send));

        LEACH_dead_Cnt = 0;
        totalArrivalDataSize = 0;
        totalArriveToSinkSize = 0;
        totalLostPacketInArrive = 0;
        totalLostPacketInTransfer = 0;
        totalTransferEnergy = 0;
        totalReceiveEnergy = 0;
	}

	for(int i = 0; i < N; i++) {
		if(!inRound) {
			node[i].x = rand() % WIDTH;
			node[i].y = rand() % HEIGHT;
			node[i].energy = GET_E_init(i);
			node[i].neighborMeanEnergy = GET_E_init(i);
			node[i].bufferSize = GET_BUF_init(i);
			node[i].cqsize = 0;
			node[i].isLive = true;
        }
		node[i].type = NODE;
		node[i].cluster_dis = INVALID_V;
		node[i].cluster_no = INVALID_V;
	}
	if(!inRound) {
		for(int i = 0; i < N; i++)
			fprintf(fanalysis, "[%d, %d] ", node[i].x, node[i].y);
		fprintf(fanalysis, "\n");
	}
    memset(cCnt, 0, sizeof(cCnt));
}

/*
 * @brief In the begining, each node sensed some data. This function give each node some data to transfer.
 * @note  not every node has data to transfer, it was controlled by PACK_INIT_RATIO, (PACK_INIT_RATIO / 100) defines the probability of having data.
 *        if PACK_INIT_RATIO == 50, then half of the nodes would have data to send. if PACK_INIT_RATIO == 100, then all nodes would have data to send.
 *        the num of data either is not fixed. is was defined by "hasPacketToTransfer % MAX_PACKET_SIZE;". You can also change it to a fixed value.
 */
void PacketArrival()
{
	int hasPacketToTransfer, dropPackSize;

	memset(packToTransfer, 0, sizeof(packToTransfer));
	for(int i = 0; i < N; i++) {
		hasPacketToTransfer = RAND() % 100;
		if(hasPacketToTransfer <= PACK_INIT_RATIO) {
            packToTransfer[i] = (hasPacketToTransfer % MAX_PACKET_SIZE);		// If you want the packet fixed, change "hasPacketToTransfer % MAX_PACKET_SIZE" to a fixed value.
            totalArrivalDataSize += packToTransfer[i];
		}
	}
	for(int i = 0; i < N; i++) {
		if(packToTransfer[i] > 0 && node[i].isLive) {
			if(node[i].bufferSize - node[i].cqsize > packToTransfer[i]) {	//enough buffer to store the packet
				node[i].cqsize += packToTransfer[i];
			}
			else {
				dropPackSize = node[i].bufferSize - node[i].cqsize;
				node[i].cqsize = node[i].bufferSize;
				totalLostPacketInArrive += dropPackSize;
			}
		}
	}
}

/*
 * @brief In LEACH, Select CH node from all nodes with arguments of LEACH_P. 
 * @note  use energy & remain buffer as auxiliary parameter to do the decision.
 */
void LEACH_selectClusterHead(int round)
{
	cluster_cnt = 0;
	for(int i = 0; i < N && cluster_cnt < 13; i++) {
		double temp_rand = (double)(RAND() % 100) / (double)100;
		double v = (LEACH_P / (1 - LEACH_P * (round % (int)((double)(1 / LEACH_P)))));		//calculate v as in LEACH algo

		if(node[i].type == NODE && node[i].isLive && node[i].energy > MIN_ENERGY_CH && 
			(node[i].bufferSize - node[i].cqsize) > MIN_BUFSIZ_CH) {
			if(temp_rand <= v) {
				node[i].type = CLUSTER_HEAD;
				cluster_id[cluster_cnt++] = i;
			}
		}
	}
}

/*
 * @brief In LEACH, after select CH node, each normal node choose its CH node to relay packets. 
 * @note  the CH node is choosen based on Q value, Q = Qsucc + Qfail.
 *        Cause the energy of CH node could change in every moment, the Q value is counted when a node is ready to send its packets.
 */
void LEACH_updateCLusterDomain(int nodeNum)		//there must have at least one cluster head
{
	int tmpId = 0, dist = INT_MAX, tmpDist;
	double maxQ = INT_MIN, Qsucc, Qfail;

	if(node[nodeNum].type == NODE && node[nodeNum].isLive) {
		Qfail = GET_Qfail(nodeNum);
		for(int j = 0; j < cluster_cnt; j++) {
			if(nodeNum != cluster_id[j]) {
				Qsucc = GET_Qsucc(nodeNum, cluster_id[j]);
                tmpDist = GET_DISTANCE(nodeNum, cluster_id[j]);
//				printf("!!{%d->%d %d;%f;%f;%f-%f��%f} ", nodeNum, j, tmpDist, maxQ, Qsucc, Qfail, D(nodeNum), D(cluster_id[j]));
				if(maxQ < Qfail + Qsucc) {
//					printf(".");
					dist = tmpDist;
					maxQ = Qfail + Qsucc;
					tmpId = j;
				}
				else if((maxQ == Qfail + Qsucc) && tmpDist < dist) {		// if Q value of two CH node is same, then choose nearer one.
//					printf("!");
					dist = tmpDist;
					maxQ = Qfail + Qsucc;
					tmpId = j;
				}
			}
		}
		node[nodeNum].cluster_dis = dist;
		node[nodeNum].cluster_no = tmpId;
		cCnt[tmpId]++;
	}
}

/*
 * @brief In LEACH, if no CH node has been selected, nodes would directly send packets to sink node. 
 * @note  the coordinate of sink node is initialized at main.
 */
void LEACH_noClusterHeadCommunicate(int nodeNum)
{
	double etx;
	int k;

	if(node[nodeNum].cqsize > 0 && node[nodeNum].isLive) {
		for(k = node[nodeNum].cqsize; k > 0; k--) {
			etx = ETX_TO_SINK(nodeNum, k);
			if(node[nodeNum].energy >= etx)
				break;
		}
		if(k < node[nodeNum].cqsize) {
			node[nodeNum].energy = 0;
			totalLostPacketInTransfer += node[nodeNum].cqsize - k;
			node[nodeNum].isLive = false;
			LEACH_dead_Cnt++;
		}
		else {
			node[nodeNum].energy -= etx;
		}
		node[nodeNum].cqsize = 0;
		totalTransferEnergy += etx;
		totalArriveToSinkSize += k;

        tx_energy[nodeNum] += etx;
        bits_send[nodeNum] += k;
	}
}

/*
 * @brief In LEACH, after a normal node choosed its CH node to relay packets. It uses this function to calculate consumption
 * @note  only normal nodes calculated in this function. comsumption of CH node would be calculated in LEACH_ClusterHeaderSend().
 */
void LEACH_ClusterHeadcommunication(int nodeNum)
{
	unsigned int itCnt = 0;
	int k;
	double etx = 0, erx = 0;

	if(node[nodeNum].type == NODE && node[nodeNum].isLive && node[nodeNum].cqsize > 0) {

		etx = ETX(nodeNum, node[nodeNum].cqsize);								// ETX needed for normal node to send all its content
		erx = ERX(cluster_id[nodeNum], node[nodeNum].cqsize);					// ERX needed for CH node to receive all content
//		printf("|_%d %d=%d %f|", GET_CLUSTER_DIST(nodeNum), nodeNum, node[nodeNum].cluster_no, GET_CLUSTER_DIST(nodeNum), GET_DISTANCE(nodeNum, GET_CLUSTER_POS(nodeNum)), etx);
		// normal node could send all content data to cluster head & CH node could receive all its content.
		if(node[nodeNum].energy >= etx && GET_CLUSTER_HEAD(nodeNum).energy >= erx) {
			k = node[nodeNum].cqsize;
			node[nodeNum].cqsize = 0;
			node[nodeNum].energy -= etx;
			GET_CLUSTER_HEAD(nodeNum).cqsize += k;
			GET_CLUSTER_HEAD(nodeNum).energy -= erx;
//			printf("@");
            tx_energy[nodeNum] += etx;
            rx_energy[GET_CLUSTER_POS(nodeNum)] +=erx;
            bits_send[nodeNum] += k;
		}

		//normal node do not have enought energy to send all its content, so calculate how many it could send.
		else if(node[nodeNum].energy < etx && GET_CLUSTER_HEAD(nodeNum).energy >= erx) {
			for(k = node[nodeNum].cqsize; k > 0; k--) {
				etx = ETX(nodeNum, k);
				erx = ERX(cluster_id[nodeNum], k);
				if(node[nodeNum].energy >= etx)
					break;
			}
			// k is the bits that node[nodeNum] could send to its CH node.
			totalLostPacketInTransfer += node[nodeNum].cqsize - k;	//only send size k to CH
//            printf("!");
			tx_energy[nodeNum] += etx;
            rx_energy[GET_CLUSTER_POS(nodeNum)] +=erx;
            bits_send[nodeNum] += k;

            node[nodeNum].energy = 0;
			node[nodeNum].cqsize = 0;
			node[nodeNum].isLive = false;
			LEACH_dead_Cnt++;
			GET_CLUSTER_HEAD(nodeNum).energy -= erx;
			GET_CLUSTER_HEAD(nodeNum).cqsize += k;
        }

		// normal node have enough energy to send while CH node do not have enough energy to receive, 
		// So calculate how many CH node could receive.
		else if(node[nodeNum].energy >= etx && GET_CLUSTER_HEAD(nodeNum).energy < erx) {
			for(k = node[nodeNum].cqsize; k > 0; k--) {
				erx = ERX(cluster_id[nodeNum], k);
				if(GET_CLUSTER_HEAD(nodeNum).energy >= erx)
					break;
			}
			totalLostPacketInTransfer += node[nodeNum].cqsize - k;	//send all cqsize while CH only received size k
 //           printf("$");
            tx_energy[nodeNum] += etx;
            rx_energy[GET_CLUSTER_POS(nodeNum)] +=erx;
            bits_send[nodeNum] += k;

            node[nodeNum].energy -= etx;
			node[nodeNum].cqsize = 0;
			GET_CLUSTER_HEAD(nodeNum).energy = 0;
			GET_CLUSTER_HEAD(nodeNum).cqsize += k;
            if(GET_CLUSTER_HEAD(nodeNum).isLive) {
				GET_CLUSTER_HEAD(nodeNum).isLive = false;
				LEACH_dead_Cnt++;
			}
		}

		// node i doesn't have enough energy to send these packets Or cluster head of node i doesn't have enough energy to receive these packets
		// So, calculate how many could be successfully send.
		else {			
			for(k = node[nodeNum].cqsize; k > 0; k--) {
				etx = ETX(nodeNum, k);
				erx = ERX(cluster_id[nodeNum], k);
				if(node[nodeNum].energy >= etx && GET_CLUSTER_HEAD(nodeNum).energy >= erx)
					break;
			}
			totalLostPacketInTransfer += node[nodeNum].cqsize - k;	//only send size k to CH
 //           printf("%");
            tx_energy[nodeNum] += etx;
            rx_energy[GET_CLUSTER_POS(nodeNum)] +=erx;
            bits_send[nodeNum] += k;

            node[nodeNum].energy = 0;
			node[nodeNum].cqsize = 0;
			node[nodeNum].isLive = false;
            LEACH_dead_Cnt++;
			GET_CLUSTER_HEAD(nodeNum).energy = 0;
			GET_CLUSTER_HEAD(nodeNum).cqsize += k;
			if(GET_CLUSTER_HEAD(nodeNum).isLive) {
				GET_CLUSTER_HEAD(nodeNum).isLive = false;
				LEACH_dead_Cnt++;
			}
		}
		totalTransferEnergy += etx;
		totalReceiveEnergy += erx;

		// if the CH node's buffer is full, in order to receive more packets, it needs to send these buffer to sink node asynchronously
		// else it will wait to be calculated in LEACH_ClusterHeaderSend()
		if(GET_CLUSTER_HEAD(nodeNum).cqsize >= GET_CLUSTER_HEAD(nodeNum).bufferSize) {

			// calculate how many bits the CH node could send, k = bits number could be send
			for(k = GET_CLUSTER_HEAD(nodeNum).bufferSize; k > 0; k--) {
				etx = ETX(nodeNum, k);
				if(GET_CLUSTER_HEAD(nodeNum).energy >= etx)
					break;
			}
			if(k < GET_CLUSTER_HEAD(nodeNum).bufferSize) {
				totalLostPacketInTransfer += GET_CLUSTER_HEAD(nodeNum).cqsize - k;
//				printf("^");
                tx_energy[GET_CLUSTER_POS(nodeNum)] += etx;
                bits_send[GET_CLUSTER_POS(nodeNum)] += k;

                GET_CLUSTER_HEAD(nodeNum).energy = 0;
				GET_CLUSTER_HEAD(nodeNum).cqsize = 0;
				if(GET_CLUSTER_HEAD(nodeNum).isLive) {
					GET_CLUSTER_HEAD(nodeNum).isLive = false;
					LEACH_dead_Cnt++;
				}
			}
			else {
//				printf("+");
                tx_energy[GET_CLUSTER_POS(nodeNum)] += etx;
                bits_send[GET_CLUSTER_POS(nodeNum)] += k;

                GET_CLUSTER_HEAD(nodeNum).energy -= etx;
				GET_CLUSTER_HEAD(nodeNum).cqsize -= GET_CLUSTER_HEAD(nodeNum).bufferSize;
			}
			totalTransferEnergy += etx;
			totalArriveToSinkSize += k;
		}
	}
//	printf("{%f : %f}", erx, etx);
}

/*
 * @brief Cluster Header send its packets to sink node. 
 * @note  check every CH node, if it has sufficient energy & has content to send, then send these packets to sink node.
 */
void LEACH_ClusterHeaderSend()
{
	double etx = 0;
	int k;

	for(int i = 0; i < cluster_cnt; i++) {
		if(!node[cluster_id[i]].isLive || node[cluster_id[i]].cqsize <= 0) {
			continue;
		}
		for(k = node[cluster_id[i]].cqsize; k > 0; k--) {
			etx = ETX_TO_SINK(cluster_id[i], k);
			if(node[cluster_id[i]].energy > etx) {
				break;
			}
		}
		if(k < node[cluster_id[i]].cqsize) {
			node[cluster_id[i]].energy -= etx;
			node[cluster_id[i]].isLive = false;
			totalArriveToSinkSize += k;
			totalLostPacketInTransfer += (node[cluster_id[i]].cqsize - k);
			LEACH_dead_Cnt++;
		}
		else {
			node[cluster_id[i]].energy -= etx;
			totalArriveToSinkSize += k;
		}
		node[cluster_id[i]].cqsize -= k;
		totalTransferEnergy += etx;
	}
}

/*
 * @brief Print statistics number to file for future analysis. 
 * @note  write throughput & energyEfficient to Result.dat. write CH node, liveNode, deadNode, energy cost & packet Lost to Analysis.dat
 */
void PrintStatistics(int round)
{
	double energyEfficiency, throughput, lostRatio;
	int sss;

	/**** print to analysis file ****/
	fprintf(fanalysis, "%03d]  clusterCnt : %d\n", round, cluster_cnt);
	fprintf(fanalysis, "cluster id        : ");
	for(sss = 0; sss < cluster_cnt; sss++) {
		fprintf(fanalysis, "%02d ", cluster_id[sss]);
	}
	fprintf(fanalysis, "\n");
	fprintf(fanalysis, "num of node in CH : ");
	for(sss = 0; sss < cluster_cnt; sss++) {
		fprintf(fanalysis, "%02d ", cCnt[sss]);
	}
	fprintf(fanalysis, "\n");
	fprintf(fanalysis, "Cluster Head node : ");
	for(sss = 0; sss < N; sss++) {
		fprintf(fanalysis, "%d ", cluster_id[node[sss].cluster_no]);
	}
	fprintf(fanalysis, "\n");
	fprintf(fanalysis, "remain energy     : ");
	for(sss = 0; sss < N; sss++) {
		fprintf(fanalysis, "%0.2f ", node[sss].energy);
	}
	fprintf(fanalysis, "\n");
	fprintf(fanalysis, "tran   energy     : ");
	for(sss = 0; sss < N; sss++) {
		fprintf(fanalysis, "%0.2f ", (double)tx_energy[sss]);
	}
	fprintf(fanalysis, "\n");
	fprintf(fanalysis, "recv   energy     : ");
	for(sss = 0; sss < N; sss++) {
		fprintf(fanalysis, "%0.2f ", (double)rx_energy[sss]);
	}
	fprintf(fanalysis, "\n");
	fprintf(fanalysis, "live node : %d    dead node : %d\n", N - LEACH_dead_Cnt, LEACH_dead_Cnt);
	fprintf(fanalysis, "total energy used in Transfer : %f J   total energy used in Receive : %f J   Sum : %f J\n", totalTransferEnergy, totalReceiveEnergy, totalTransferEnergy + totalReceiveEnergy);
	fprintf(fanalysis, "total Bytes lost in transfer  : %ld B   total Bytes lost in arrive : %ld B\n", totalLostPacketInTransfer, totalLostPacketInArrive);
	fprintf(fanalysis, "total Bytes arrived in sink   : %ld B   total Bytes arrived : %ld B\n\n", totalArriveToSinkSize, totalArrivalDataSize);

	/**** print to result file ****/
	lostRatio = (double)(totalLostPacketInTransfer + totalLostPacketInArrive) / (double)totalArriveToSinkSize;
	throughput = (double)1 - lostRatio;
	energyEfficiency = (double)totalArriveToSinkSize / (double)(totalReceiveEnergy + totalTransferEnergy);

	fprintf(fresult, " %03d\t  %0.5f\t       %0.5f\t        %0.5f\n", round, throughput, energyEfficiency, meanResidualEnergy);
	printf("%02d) dead Cnt : %02d    Cluster Num : %02d\n", round, LEACH_dead_Cnt, cluster_cnt);
}



void LEACH_round()
{
	// ROUND defines the round of algo procedure.
	for(int i = 0; i < ROUND; i++) {
		LEACH_Init(true);

		PacketArrival();
        LEACH_selectClusterHead(i);
		for(int j = 0; j < N; j++) {
			if(cluster_cnt > 0) {


				LEACH_updateCLusterDomain(j);
//				printf("[2]%d ", LEACH_dead_Cnt);

				LEACH_ClusterHeadcommunication(j);
//				printf("[3]%d ", LEACH_dead_Cnt);
			}
			else {
				LEACH_noClusterHeadCommunicate(j);
			}
			updateNeighborMeanResEnergy(j);
		}
		LEACH_ClusterHeaderSend();
		updateMeanResidualEnergy();
		PrintStatistics(i);
	}
}

int main()
{
	srand((unsigned int)time(NULL));
	sinkNode.x = WIDTH / 2;
	sinkNode.y = HEIGHT / 2;
	Init_OutFile();

	LEACH_Init(false);
	LEACH_round();
	Clost_OutFile();

	return 0;
}
